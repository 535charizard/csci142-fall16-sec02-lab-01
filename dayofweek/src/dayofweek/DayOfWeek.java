package dayofweek;

public class DayOfWeek {
	/**
	 * The class DayOfWeek stores the value of a given
	 * date and returns the given month for that date and the day of the week for the given date. 
	 *@author DeArvis Troutman 
	 *@author DeArvis Troutman
	 *
	 *September 21st midnight
	 *
	 *The information required to run this class is date with a day of the month, month,
	 *and year.
	 *The output of the class is the month corresponding to the date as well as the day 
	 *of the week.
	 *
	 */
	public final static int NO_VALUE = -1;
	public int DOW;
	public int Year;
	public int mos;
	public int DayOfMonth;
	public String[] DaysOfWeekArray = new String [12];
	public String [] GetMonthStringArray = new String [15];
	/**
	 * Method to store date with the following parameters
	 * 
	 * @param DayOfMonth
	 * @param mos
	 * @param year
	 * 
	 * @DeArvis Troutman
	 */
	public DayOfWeek(int DayOfMonth,int mos,int year)
	{
		if(year <1900 || year >2099)
		{
			mos = 13;
			DayOfMonth = 9;
		}
			    this.Year = year;
			    this.mos = mos;
			    this.DayOfMonth = DayOfMonth;
	}

	public int getYear() 
	{
		
		return Year;
	}

	public int getMonth1() 
	{
		return mos;
	}

	public int getDayOfMonth() 
	{
		return DayOfMonth;
	}
	/**
	 * This method has an array with a list of days of the week.
	 * 
	 * @return
	 * 
	 * @DeArvis Troutman
	 */
	public String[] setupDayOfWeek() 
	{
		DaysOfWeekArray[0]= "Saturday";
		DaysOfWeekArray[1]= "Sunday";
		DaysOfWeekArray[2]= "Monday";
		DaysOfWeekArray[3]= "Tuesday";
		DaysOfWeekArray[4]= "Wednesday";
		DaysOfWeekArray[5]= "Thursday";
		DaysOfWeekArray[6]= "Friday";
		return DaysOfWeekArray;
	}
	/**
	 * This Method calculates the day of the week for the date 
	 * and compares the calculated day of the week to the day of the week
	 * in the DaysOfWeekArray.
	 * 
	 * @return
	 * 
	 * @DeArvis Troutman
	 */
	public int NumericDayOfWeek()
	{	 
		  double m;
		  int y;
	      int DOW;
		  m = (mos -2 + 12) % 12;
		  y = 5 * (Year % 4) + 4 * (Year % 100) + 6 * (Year %400);  
		  DOW = (DayOfMonth + (int)(2.6 *m - 0.2) + y) % 7 ;
	      this.DOW = DOW;
		  if (mos<= 2)
			{
		       Year--;
			}
               if(this.Year <1900 || this.Year >2099 || this.mos < 1 || this.mos >12 || this.DayOfMonth >1|| this.DayOfMonth <32)
               {
            	   DOW = NO_VALUE;
               }
                      return DOW;
	}
		/**
		 * This method fills the elements in GetMonthStringArray with a month.
		 * 
		 * @return
		 * 
		 * @DeArvis Troutman
		 */
	public String[] getMonthString()
	{	        
		GetMonthStringArray[0] = null;
		GetMonthStringArray[1] = "January";
		GetMonthStringArray[2] = "February";
		GetMonthStringArray[3] = "March";
		GetMonthStringArray[4] = "April";
		GetMonthStringArray[5] = "May";
		GetMonthStringArray[6] = "June";
		GetMonthStringArray[7] = "July";
		GetMonthStringArray[8] = "August";
		GetMonthStringArray[9] = "September";
		GetMonthStringArray[10] = "October";
		GetMonthStringArray[11] = "November";
		GetMonthStringArray[12] = "December"; 	
		  if(this.Year < 1900 || this.Year > 2099 || this.mos < 1 || this.mos >12 || this.DayOfMonth < 1|| this.DayOfMonth > 32)
          {
         	   GetMonthStringArray = null;
          }
		           return GetMonthStringArray;
     }	
			/**
			 * This method compares the month from the date and compares it with
			 * the month from GetMonthStringArray.
			 * 
			 * @return
			 * 
			 * @DeArvis Troutman
			 */
	public int getMonth()
	{
		int IntMonth = this.mos;
		   if(this.Year <1900 || this.Year >2099 || this.mos < 1 || this.mos >12 || this.DayOfMonth < 1|| this.DayOfMonth > 32)
	       {
	         IntMonth = NO_VALUE;
	         System.out.println("null");
	       }
		       else
		        {
		          System.out.println(GetMonthStringArray[IntMonth]);
	            } 
                      return IntMonth;
	}
    /**
     * This method checks to see if the input date has a valid month, and if it doesn't then
     * returns answer as null.
     * 
     * @return
     * 
     * @ DeArvis Troutman
     */
	public Object getDayOfWeek()
	{	
	    if(this.mos > 12 || this.mos < 0)
		{
			DOW = 11;
		}
	            System.out.println(DaysOfWeekArray[DOW]);
		 		return DaysOfWeekArray[DOW];	
	}
    
	public int getNumericDayOfWeek()
	{
    	
	return NumericDayOfWeek();
	}
	}
	
			 



