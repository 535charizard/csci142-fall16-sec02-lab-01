package dayofweektesting;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Test suite constructed of all test cases in "testcases" source folder
 * @author dplante
 *
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { 
	TestInvalidDates.class,
	TestValidLeapYears.class,
	TestValidNonLeapYears.class})
public class AllTests {
	// class remains empty and serves as a place holder for above
}
