package dayofweektesting;

import dayofweek.DayOfWeek;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;

/**
 * Test cases for "Day of week" lab assuming requirements set 
 * at top of DayOfWeek class
 * 
 * Test invalid boundary dates, dates treated as leap years (but are not),
 * and invalid days for months.
 * 
 * @author dplante
 *
 */

public class TestInvalidDates {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	/**
	 * Test invalid date before first valid date
	 */
	@Test
	public void beforeFirstDay() {
		DayOfWeek dow = new DayOfWeek(12, 31, 1899);
		this.assertBadDOW(dow);
	}
	
	/**
	 * Test invalid date after last valid date
	 */
	@Test
	public void afterLastDay() {
		DayOfWeek dow = new DayOfWeek(1, 1, 2100);
		this.assertBadDOW(dow);
	}

	/**
	 * Test invalid date of Feb. 29th when not leap year
	 * - year right after a leap year
	 */
	@Test
	public void notLeapYearAfter() {
		DayOfWeek dow = new DayOfWeek(2, 29, 1909);
		this.assertBadDOW(dow);
	}

	/**
	 * Test invalid date of Feb. 29th when not leap year
	 * - year right between leap years
	 */
	@Test
	public void notLeapYearMiddle() {
		DayOfWeek dow = new DayOfWeek(2, 29, 1910);
		this.assertBadDOW(dow);
	}

	/**
	 * Test invalid date of Feb. 29th when not leap year
	 * - year right after a leap year
	 */
	@Test
	public void notLeapYearBefore() {
		DayOfWeek dow = new DayOfWeek(2, 29, 1911);
		this.assertBadDOW(dow);
	}
	
	/**
	 * Test invalid 30th and 31st-day-of-month dates.
	 * 		2/30/2000
	 * 		2/31/2000
	 * 		4/31/2000
	 * 		6/31/2000
	 * 		9/31/2000
	 * 		11/31/2000
	 */
	@Test
	public void badFebruary30() {
		DayOfWeek dow = new DayOfWeek(2, 30, 2000);
		this.assertBadDOW(dow);
	}
	
	@Test
	public void badFebruary31() {
		DayOfWeek dow = new DayOfWeek(2, 31, 2000);
		this.assertBadDOW(dow);
	}
	
	@Test
	public void badApril() {
		DayOfWeek dow = new DayOfWeek(4, 31, 2000);
		this.assertBadDOW(dow);
	}
	
	@Test
	public void badJune() {
		DayOfWeek dow = new DayOfWeek(6, 31, 2000);
		this.assertBadDOW(dow);
	}
	
	@Test
	public void badSeptember() {
		DayOfWeek dow = new DayOfWeek(9, 31, 2000);
		this.assertBadDOW(dow);
	}
	
	@Test
	public void badNovember() {
		DayOfWeek dow = new DayOfWeek(11, 31, 2000);
		this.assertBadDOW(dow);
	}

	/**
	 * Test invalid day-of-month (negative)
	 */
	@Test
	public void badDOMneg() {
		DayOfWeek dow = new DayOfWeek(1, -1, 2000);
		//               
		assertTrue(dow.getMonth() == DayOfWeek.NO_VALUE);
		assertTrue(dow.getMonthString() == null);
		this.assertBadDOW(dow);
	}

	/**
	 * Test invalid day-of-month (0)
	 */
	@Test
	public void badDOM0() {
		DayOfWeek dow = new DayOfWeek(1, 0, 2000);
		assertTrue(dow.getMonth() == DayOfWeek.NO_VALUE);
		assertTrue(dow.getMonthString() == null);
		this.assertBadDOW(dow);
	}

	/**
	 * Test invalid day-of-month (32)
	 */
	@Test
	public void badDOMafter() {
		DayOfWeek dow = new DayOfWeek(1, 32, 2000);
		assertTrue(dow.getMonth() == DayOfWeek.NO_VALUE);
		assertTrue(dow.getMonthString() == null);
		this.assertBadDOW(dow);
	}

	/*
	 * Private helper method to check returned int and String values.
	 */
	private void assertBadDOW(DayOfWeek dow)
	{
		assertTrue(dow.getDayOfWeek() == null);
		assertTrue(dow.getNumericDayOfWeek() == DayOfWeek.NO_VALUE);
	}

}
